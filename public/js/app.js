'use strict';
var app=angular.module("apprecetas",["ngRoute"]);
app.config(function($routeProvider){
	$routeProvider.when("/",{templateUrl:"view/login.html",controller:"loginController"})
	.when("/admin",{templateUrl:"view/dashboardAdmin.html",controller:"adminController"})
	.when("/clase",{templateUrl:"view/clase.html",controller:"claseController"})
	.when("/producto",{templateUrl:"view/producto.html",controller:"productoController"})
	.when("/user",{templateUrl:"view/dashboardUser.html",controller:"userController"});
});	