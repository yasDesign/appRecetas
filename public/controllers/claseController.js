angular.module("apprecetas").controller("claseController", function($scope, $http) {
    $scope.pagina = "clases";
    $scope.clase = [];
    $scope.clases = [];
    actualizarVista();
    $scope.filesArray = []

    function actualizarVista() {

        $http.get("/getallclases", {}).then(function(resutl) {
            console.log(resutl.data);
            $scope.clases = resutl.data;
        });
    }

    $scope.guardarClase = function() {
        console.log($scope.nombre);

        var formdatos = new FormData();
        for (key in $scope.clase) {
            console.log(key + ":");
            console.log($scope.clase[key]);
            formdatos.append(key, $scope.clase[key]);
        }

        var file = $("#file")[0].files[0];
        formdatos.append("imagen", file);
        var config = { transformRequest: angular.identity, headers: { 'Content-type': undefined } };
        $http.post("/setclase", formdatos, config).then(function(result) {
            console.log(result.data);
            actualizarVista();
            $scope.clase = [];
            alert("clase neva ha sido ingresada");
        });
    };

    $scope.eliminarClase = function(idclase) {
        $http.post("/deleteclase",{"_id":idclase}).then(function(result){
            console.log("dato elimnado");
            alert("elimnado item");
            actualizarVista();
        });
        
    };
});