angular.module("apprecetas").controller("productoController",function($scope,$http){
	$scope.pagina="productos";
	$scope.producto=[];
	$scope.productos=[];
	actualizarVista();
	$scope.clases=[];

	getClases();

	function getClases(){
		$http.get("/getallclases",{}).then(function(result){
			$scope.clases=result.data;
		});
	}

	function actualizarVista(){
		$http.get("/getallproductos",{}).then(function(result){
			$scope.productos=result.data;
		});
	}

	$scope.guardarProducto=function(){
        var formdatos = new FormData();
        for (key in $scope.producto) {
            console.log(key + ":");
            console.log($scope.producto[key]);
            formdatos.append(key, $scope.producto[key]);
        }
        var file = $("#file")[0].files[0];
        formdatos.append("imagen", file);
        var config = { transformRequest: angular.identity, headers: { 'Content-type': undefined } };
        $http.post("/setproducto", formdatos, config).then(function(result) {
            console.log(result.data);
            actualizarVista();
            $scope.producto = [];
            alert("producto neva ha sido ingresada");
        });
	}

	$scope.eliminarProducto=function(idpro){
		$http.post("/deleteproducto",{"_id":idpro}).then(function(result){				
			actualizarVista();
			alert("producto eliminado");
		});
	}

});