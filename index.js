var express = require("express");
var bodyParser = require("body-parser");
var app = express();
var multer = require("multer");
var upload = multer({ dest: "upload/" });
var fs = require("fs");
var mongoose = require("mongoose");

mongoose.Promise = global.Promise;

mongoose.connect('mongodb://sw:admin@ds157288.mlab.com:57288/appreceta', { useMongoClient: true }).then(() =>
    console.log("conection successful!!")).catch((err) => console.error(err));

var Schema=mongoose.Schema;

var clasesSchema = new Schema({ nombre: String, descripcion: String, imagen: String });
var clasesModel = mongoose.model('clases', clasesSchema);

var productosSchema=new Schema({nombre: String, descripcion: String, imagen: String ,idclase:{type:Schema.ObjectId,ref:"clases"}});
var productosModel=mongoose.model('productos',productosSchema);


// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));
// parse application/json
app.use(bodyParser.json());

app.use(express.static(__dirname + "/public"));
app.set("port", (process.env.PORT) || 5000);

////******************clase*********************////
app.post("/setclase", upload.any(), function(req, res) {
    console.log("body:" + JSON.stringify(req.body, null, 2));
    if (req.files) {
        req.files.forEach(function(file) {
            var filename = (new Date).valueOf() + "-" + file.originalname;
            fs.rename(file.path, "public/uploads/" + filename, function(err) {
                if (err) throw err;
                ///guardar en database
                console.log("file upload");

                var clase=new clasesModel({nombre:req.body.nombre,descripcion:req.body.descripcion,imagen:filename});
				clase.save(function(err){
					if(err){
						console.log("no guardado");
			            res.send("no ok");
					}else{
					 	console.log("producto guardado");
			            res.send("ok");
					}
				});
            });

        });
    }
});

app.get("/getallclases", function(req, res) {
    clasesModel.find({}, function(err, users) {
        if (err) throw err;
        res.send(JSON.stringify(users, null, 2));
    });
});

app.post("/deleteclase",function(req,res){
    clasesModel.remove(req.body,function(err,result){
        if (err) {
            console.log('dato eliminado');
        }else{
            res.send("ok");
        }
    });
});

//******************************productos**************************************/////
app.post("/setproducto", upload.any(), function(req, res) {
    console.log("body:" + JSON.stringify(req.body, null, 2));
    if (req.files) {
        req.files.forEach(function(file) {
            var filename = (new Date).valueOf() + "-" + file.originalname;
            fs.rename(file.path, "public/uploads/" + filename, function(err) {
                if (err) throw err;
                ///guardar en database
                console.log("file upload");

                var producto=new productosModel({nombre:req.body.nombre,descripcion:req.body.descripcion,imagen:filename,idclase:req.body.idclase});
                producto.save(function(err){
                    if(err){
                        console.log("no guardado");
                        res.send("no ok");
                    }else{
                        console.log("producto guardado");
                        res.send("ok");
                    }
                });
            });

        });
    }
});

app.get("/getallproductos",function(req,res){
    productosModel.find({},function(err,result){
        if (err) {console.log("error consulta");}
        res.send(JSON.stringify(result,null,2));
    });
});

app.post("/deleteproducto",function(req,res){
    productosModel.remove(req.body,function(err,result){
        if (err) {console.log("error consulta");}
        res.send("ok");
    });
});

app.listen(app.get("port"), function() {
    console.log("server run in 5000 port");
});